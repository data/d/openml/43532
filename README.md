# OpenML dataset: The-Social-Dilemma-Tweets---Text-Classification

https://www.openml.org/d/43532

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Social Dilemma, a documentary-drama hybrid explores the dangerous human impact of social networking, with tech experts sounding the alarm on their own creations as the tech experts sound the alarm on the dangerous human impact of social networking.
Initial release: January 2020
Director: Jeff Orlowski
Producer: Larissa Rhodes
Music director: Mark A. Crawford
Screenplay: Jeff Orlowski, Vickie Curtis, Davis Coombe
Content
This dataset brings you the twitter responses made with the TheSocialDilemma hashtag after watching the eye-opening documentary  "The Social Dilemma" released in an OTT platform(Netflix) on September 9th, 2020.
The dataset was extracted using TwitterAPI, consisting of nearly 10,526 tweets from twitter users all over the globe!



No
Columns
Descriptions




1
user_name
The name of the user, as theyve defined it.


2
user_location
The user-defined location for this accounts profile.


3
user_description
The user-defined UTF-8 string describing their account.


4
user_created
Time and date, when the account was created.


5
user_followers
The number of followers an account currently has.


6
user_friends
The number of friends an account currently has.


7
user_favourites
The number of favorites a account currently has


8
user_verified
When true, indicates that the user has a verified account


9
date
UTC time and date when the Tweet was created


10
text
The actual UTF-8 text of the Tweet


11
hashtags
All the other hashtags posted in the tweet along with TheSocialDilemma


12
source
Utility used to post the Tweet, Tweets from the Twitter website have a source value - web


13
is_retweet
Indicates whether this Tweet has been Retweeted by the authenticating user.


14
Sentiment(Target variable)
Indicates the sentiment of the tweet, consists of three categories: Positive, neutral, and negative



Inspiration
You can use this data to dive into the subjects that use this hashtag, look to the geographical distribution, evaluate sentiments, looks to trends.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43532) of an [OpenML dataset](https://www.openml.org/d/43532). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43532/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43532/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43532/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

